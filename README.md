# houselisting

Kalle, Jessica & Andreas

## Required modules
```
npm install -D sass-loader node-sass
npm install --save vue-observe-visibility
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
