const Data = {
    agents: require('../assets/data.json'),

    getListing(agentId, listingId) {
        return this.agents[agentId].listings[listingId]
    },

    getAgent(agentId) {
        return this.agents[agentId]
    }
} 

export default Data