import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { ObserveVisibility } from 'vue-observe-visibility'

Vue.config.productionTip = false
Vue.directive('observe-visibility', ObserveVisibility)
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')