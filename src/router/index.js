import Vue from 'vue'
import VueRouter from 'vue-router'
import Agent from '../views/Agent.vue'
import Index from '../views/Index.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Index',
    component: Index
  },

   {
    path: '/house/:houseid/:agentid',
    name: 'House',
    component: () => import('../views/HouseInfo.vue')
  }, 
  {
    path: '/agent/:id',
    name: 'Agent',
    component: Agent
  }

]

const router = new VueRouter({
  routes
})

export default router
